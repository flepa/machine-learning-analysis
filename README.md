# Computational and Statistical Learning Project

This university project analyzes the **CIFAR10** dataset and also applies on it some machine learning methodologies based on samples classification.

## Objectives

- Dataset analysis and description (**CIFAR10**).

- **Preprocessing** and **dimensionality reduction** (**PCA** and **LDA**) stages on the dataset.

- Preprocessed data and original data **comparison** in order to verify if the choices made are correct. This is achieved with three machine learning methods:

- Logistic Regression

- Ensemble method (Random Forest)

- Convolutional Neural Network

These are three models with different *philosophies* and mechanisms, it should be useful to analyze their behaviour with the available datasets.
